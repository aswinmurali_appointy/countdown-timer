import { Component } from "react";
import './Clock.css';

interface ClockState {
    days: number;
    hours: number;
    minutes: number;
    seconds: number;
}

class Clock extends Component<ClockState> {
    state: ClockState;
    interval: NodeJS.Timer | null;

    constructor(props: ClockState) {
        super(props);
        this.state = props;
        this.interval = null;
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        if (this.interval != null)
            clearInterval(this.interval);
    }

    tick = () => {
        this.setState({ seconds: this.state.seconds - 1 })

        if (this.state.seconds < 1) {
            this.setState({ seconds: 59, minutes: this.state.minutes - 1 })
        }
        if (this.state.minutes < 1) {
            this.setState({ minutes: 59, hours: this.state.hours - 1 })
        }
        if (this.state.hours < 1) {
            this.setState({ hours: 23, days: this.state.days - 1 })
        }

        if (this.state.days === -1) {
            this.setState({ days: 0 })
        }
        
        if (this.state.seconds <= 0 && this.state.minutes <= 0 && this.state.hours <= 0 && this.state.days <= 0) {
            if (this.interval != null)
                clearInterval(this.interval);

            this.reset();
        }
    };

    reset = () =>
        this.setState({ days: 0, hours: 0, minutes: 0, seconds: 0 });

    render() {
        return (
            <table id='counter' style={{
                justifyContent: 'center',
                display: 'flex',
                fontSize: '40px'
            }} cellPadding={10}>
                <tr>
                    <th>
                        <tr><th className='count'>{this.state.days}</th></tr>
                        <tr><th>Days</th></tr>
                    </th>
                    <th>
                        <tr><th className='count'>{this.state.hours}</th></tr>
                        <tr><th>Hours</th></tr>
                    </th>
                    <th>
                        <tr><th className='count'>{this.state.minutes}</th></tr>
                        <tr><th>Mins</th></tr>
                    </th>
                    <th>
                        <tr><th className='count'>{this.state.seconds}</th></tr>
                        <tr><th>Secs</th></tr>
                    </th>
                </tr>
            </table>
        );
    }
}

export default Clock;
