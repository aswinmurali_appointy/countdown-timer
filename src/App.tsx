import './App.css';

import Clock from './Clock/Clock';

const notImplemented = () => alert('Not Implemented');
const reloadApp = () => window.location.reload();

const App = () => (
  <>
    <div className='theme'>
      <div style={{
        float: 'left',
        width: '50%',
        fontSize: '40px',

      }}>Countdown Timer</div>
      <div style={{
        float: 'right',
        width: '50%',
        textAlign: 'end',
        alignItems: 'center',
      }}>
        <input type={'button'} value={'Reset'} onClick={reloadApp}></input>
        <input type={'button'} value={'Settings'} onClick={notImplemented}></input>
      </div>
    </div>

    <div style={{
      textAlign: 'center',
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center',
      fontSize: '40px'
    }}><p>Countdown ends in ...</p></div>

    <Clock days={1} hours={2} minutes={0} seconds={0} />
  </>
)


export default App;
